Pod::Spec.new do |s|
  s.name             = 'BlocksKit'               #名称
  s.version          = '2.2.6'                       #版本号
  s.summary          = 'The Objective-C block utilities you always wish you had.' # #简短介绍，下面是详细介绍

  s.description      = <<-DESC
The Objective-C block utilities you always wish you had.
                       DESC

 #主页,这里要填写可以访问到的地址，不然验证不通过，注意必须为http开头，不然无法通过pod lib lint检验通过
  s.homepage         = 'https://gitlab.com/GucciPrada/blockskit.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = 'MIT'#开源协议
  s.author           = { 'b3s' => 'sdkcustomer@body3dscale.com' }
 #项目地址，这里不支持ssh的地址，验证不通过，只支持HTTP和HTTPS，最好使用HTTPS
  s.source           = { :git => 'https://gitlab.com/GucciPrada/blockskit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0' #支持的平台及版本

  s.source_files = 'BlocksKit/**/*'
  s.requires_arc = false  #是否使用ARC，如果指定具体文件，则具体的问题使用ARC
  s.frameworks = 'UIKit'
  # s.resource_bundles = {
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'#公开头文件地址
  # s.frameworks = 'UIKit', 'MapKit' #所需的framework，多个用逗号隔开
  # s.dependency 'AFNetworking', '~> 2.3' #依赖关系，该项目所依赖的其他库，如果有多个需要填写多个s.dependency
end

