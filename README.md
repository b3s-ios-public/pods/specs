## 这是什么?
这是自制pods的specs索引库,保存各种自制库的podspec. 简称自制pod源.

## 如何制作一个自制pod库并发布？
以SDKIdentiy库为例子，介绍如何构建并发布一个pod库

-----
#### 1. 安装cocoapods工具
  ```shell
  #先确保本地已经安装好了cocoapods，网上搜如何安装这个工具，若已安装则跳过此步
  sudo gem install cocoapods
  ```

#### 2. 为cocoapods添加公司内部的一个源
 ```shell
 #如果已添加该源，则可以忽略这一步
 pod repo add mypod-spec https://gitlab.com/ios-scan/pods/specs.git
 ```

#### 3. 给libSDKIdentity工程打标签并发布到上述私有pod源

  ```shell
  #将SDKIdentity源码工程中打好的静态framework拷贝、覆盖libSDKIdentity工程下的同名framework
  #更新libSDKIdentity.podspec中的版本号，通常是1.2或1.2.1这样的格式
  git add ./
  #commit所有的修改
  git commit
  #给master分支打标签, 注意标签名字和libSDKIdentity.podspec版本号要一样
  git tag -a <版本号,如1.2.1> -m '<说点什么>'
  #push master到远程仓库
  git push origin <版本号,如1.2.1>
  #发布本工程的podspec到office-specs源
  ./publish.sh
  ```
